**Moore's law** is a historical trend of [[integrated circuit|IC]]
development: the density of transistors can be doubled every two years. It
was posited by Gordon Moore in 1965. Several experts, including Moore
himself, have predicted that the trend will end by 2025.

While there are definitely good aspects in Moore's law (including the
improvement of energy and material efficiency), it has also come with
economical, technological and philosophical side effects.

Perhaps the most prominent of these is an extreme form of
[[Jevons paradox]]: a dramatic increase in resource efficiency has led to a
dramatic increase in resource use. In software, Jevons paradox manifests as
[[Wirth's law]]: software becomes slower (more bloated, less
energy-efficient) more rapidly than hardware becomes faster (more
energy-efficient). Jevons paradox is also a major driver of
[[obsolescence]].

Moore's law is not a "law of physics" but depends on inreasing economic
investments: **Moore's second law**, also known as **Rock's law**, states
that the cost of a state-of-the-art fabrication plant doubles every four
years. The smaller the feature size, the more specialization and complexity
is needed in equipment and processes. This has lead to extreme
[[centralization]] of fabs.

Philosophically, Moore's law has given a lot of room to [[maximalism]],
including the idea that technological progress is mostly increase of
quantity rather than change of quality. The emphasis on quantitative
maximization easily leads to maximization of energy and resource use.

Permacomputing welcomes efficiency improvements without the afore-mentioned
side effects. It therefore emphasizes [[lifespan maximization]], small-scale
and local production, [[minimization]] and non-maximalist [[aesthetics]].

Gordon Moore is not to be confused with Chuck Moore, the creator of
[[Forth]], nor with Max More, a proponent of [[extropianism|maximalism]].
