Jevons paradox
==============

**Jevons paradox** refers to the phenomenon where the increase of efficiency
in the use of a resource leads to more use of the resource. Jevons
originally noticed in 1865 that the development of more fuel-efficient steam
engines resulted in an increased total use of coal: the falling cost of coal
increased its demand and negated the gains.

See also:

* [[Wirth's law]] - a computing-specific variant of Jevons paradox
