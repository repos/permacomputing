##Research methods

###Livinglabs

Sustainability research method, experimental/development environment and ecosystem.

Local initiatives, pragmatic and efficient, balancing social, environmental and economical considerations. Livinglabs are places to overcome  narrow scientific or engineering specialization and theoretical assumptions and test consequences of technology next to living ecosystems. All actors - human, animal, plant or microbiome and researchers are on the same level to resolve positive or negative impacts. Instead observation of subjects there is principle of co-creation, experiential learning, prototyping involving communities, inclusive social space for designing and experiencing their own future.

###Artistic research

Creative inquiry, practice as research.

Practice led research is a distinctive feature of the research
activity conducted by arts and humanities researchers,
it involves the identification of research questions and
problems, but the research methods, contexts and outputs
then involve a significant focus on creative practice.
This type of research thus aims, through creativity and
practice, to illuminate or bring about new knowledge and
understanding, and it results in outputs that may not be
text-based, but rather a performance.

###Scientific critique, interdisciplinarity

Interdisciplinarity means not only multiple disciplines involved (informatics, biology...), but also multiple domains of inquiry.

* *Interpretivist:* meaning, constructivist, network, dialogue, interdiscipline
* *Empiricist:* exploratory, conceptual, reflective, discipline-based
* *Critical:* positionality-change, contextual, transdiscipline, perspective, question
* *Art practice:* meta-theoretical, practical, reflexive, post-discipline, visual systems

Art practice also come in coupled with critical, interpretivist and empiricist domains in theory dimensions: 

* Create-critique, Meaning-making, Enact-explain

##Research fields

Meta-disciplines of trans-discipline of sustainability:

###Ecosystems and computational conditions of biodiversity

###Sustainability and toxicity of computation

###Biodigitality and bioelectric energy



