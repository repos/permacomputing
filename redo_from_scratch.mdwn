**Redo from scratch** is an idea that is antithetical to [[reuse]]. Instead
of modifying an existing program to fit a new purpose, a completely new
program is written from scratch.

The philosophy of Chuck Moore, the author of [[Forth]], has very strong RFS
elements. Instead of deciding a set of standard pieces to build software
from, Moore was ready to rethink just about any previous decision. What came
to be the time-tested core of the Forth language was based on constant and
obsessive rethinking, experimentation and redoing-from-scratch.

In practical purposes, RFS often has huge risks and problems, starting from
the bugginess typical of new programs. However, it can be heartily
recommended as an artistic or educational practice, as a way to exercise
one's programming skills, or as a research method for experimenting with
completely new ideas.
