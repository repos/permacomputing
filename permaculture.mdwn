Permaculture
============

**Permaculture** is an approach to land management and settlement design that adopts arrangements observed in flourishing natural ecosystems. First formulated in 1978, it particularly stands in opposition against industrial agriculture. [[Permacomputing]] is based on the idea of applying permacultural ideas to computing (and "high" technology in general).

In particular, permaculture inspires permacomputing to:

  * Recognizing the effects of computing to the biosphere, and trying to find ways to make these effects positive and regenerative.
  * Turning waste into resources and constraints into possibilities.
  * Explorative, imaginative and positive attitudes towards sustainable design, as opposed to "returning to the past" or "having to tolerate lesser resources".
  * Opposition to the mainstream technological industry while offering a tangible alternative.

Permacomputing isn't the first attempt to bridge permaculture and computing. Earlier examples include:

  * [The Permaculture entry on WikiWikiWeb](http://wiki.c2.com/?PermaCulture) connects it with software design patterns but does not connect to the ecological reality.
  * Kent Beck's talk "Programming as a garden: Permaprogramming" similarly drew inspiration from the philosophy to software design without the ecological aspect.
  * [Amanda Starling Gould](https://amandastarlinggould.com/research/)'s 2017 doctoral dissertation ["Digital Environmental Metabolisms: An Ecocritical Project of the Digital Environmental Humanities"](https://dukespace.lib.duke.edu/dspace/handle/10161/14457) centers around the ecological aspect but concentrates on end-user activities.
