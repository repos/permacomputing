**Smallnet** or **SmolNet** (also known as Small/Smol Internet/Web, etc.) is
a movement of small-scale Internet networking that emphasizes the smallness
of the servers (including low system requirements) as well as that of the
user communities. It can be regarded as a counter-movement to the
centralization and bloatedness of [[WWW]].

Examples of Smallnet:
  * [[Gemini]]
  * Public [[Unix]] servers (see also [[time-sharing]])
  * [[BBS]] communities
