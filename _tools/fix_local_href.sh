#!/bin/sh
#
# if you call ikiwiki directly to generate HTML files to browse locally
# you will be disapointed as ikiwiki links points to folder in which an
# index.html will be found. This is fine for an HTTP server as most will
# default sending the index.html to the browser upon folder URL request.
# However, if you browse locally, the web browser will display the content
# of the folder instead. Making navigation super annoying. This script to
# be run everytime a new static version is built will fix all the href to
# actually point to the index.html instead of pointing to the folder.


# I don't trust you
if [ -z "${1}" -o ! -e "${1}/ikiwiki" ]
then
  echo "[!] please point to ikiwiki HTML folder"
  echo "[.] usage ./fix_local_href.sh some/path/to/html_wiki"
  exit 1
else
  INDEXES="$(find ${1} -iname index.html)"  # inb4 -exec jaja nope
  for INDEX in ${INDEXES}
  do
    echo ${INDEX}
    sed -i 's/href="\(\(\|\/\|.\/\|..\/\)[[:alnum:]_-]*\(\|\/\)\)">/href="\1index.html">/g' ${INDEX}
  done
fi


