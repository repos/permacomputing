**Forth** is a [[stack-based]] programming language created in the 1970s by Chuck Moore.

The main benefit of Forth is that it is very small. A classical Forth system takes the roles of the compiler, the editor and the operating system while completely fitting in a memory space of less than 20 kilobytes. The smallness also makes it possible to implement a Forth system from scratch in a weekend.
                             
Forth is often run with a two-stack [[virtual machine]], which makes it much slower than compiled code, although native-compiling Forth systems are also common. Classical Forth systems typically also include an [[assembler]] that can be used to implement speed-critical parts of the program.

Most programmers see Forth as quite esoteric in comparison to other languages. Forth also doesn't "protect" the programmer from its inner peculiarities – even though it is possible to create abstractions, it is not advisable to forget what lies under those abstractions.

Forth has been standardized, but Moore himself hasn't cared so much about standardization. The "[[Redo from scratch]]" ideal is quite strong in the Forth culture and exemplified by Moore's own quest for an optimal set of language elements.

See also:

* [Thinking Forth (the classical Forth book)](http://thinking-forth.sourceforge.net/)
* [Forth - the Early Years (by Chuck Moore)](https://colorforth.github.io/HOPL.html)
* [Standard Forth system for Uxn](http://www.call-with-current-continuation.org/uf/uf.html)
