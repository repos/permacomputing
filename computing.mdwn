**Computing** refers to the kind of data-processing activities that can be
[[automated|automation]] by computer [[hardware]], as well as all kinds of
other activities related to the hardware. Originally, the term was
synonymous with counting and calculating.

Permacomputing has so far been mostly concerned about computer-based
computing rather than things like mental calculation.

It should also be noted that even though many problems are currently solved
with computing or digital technology, this does not always need to be the
case. Computing has its strengths and weaknesses compared to [[non-digital]]
technologies – for example, in long-term archival of documents,
[[microform]]s may be a very feasible alternative and should therefore be
considered.
