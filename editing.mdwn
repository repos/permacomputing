Style
-----

Permacomputing wiki is not [[Wikipedia]], so being neutral or encyclopedic is
not among our goals, and original research is encouraged. However, it is a
collaborative project, so if you want to express an opinion the other editors
may not agree with, please use the relevant Discussion page, and make sure to
sign your comment with your handle. Go to the Discussion page, in the top menu
of this page for an example.


Copyediting recommendations
---------------------------


1. explain the abbreviations before using it (ex. Operations Systems (OS) are
   amazing. The OS are actually shit).

2. make sure the references are explained before using them. (ex. "Unix is a
   multi-user operating system whose development was started in 1969 by Ken
   Thompson and Dennis Ritchie, as well as an entire family of operating system
   derived from the original Unix." First, explain the original Unix, or
   rearrange the sentences where you explain what unix is, then explain that
   there are multiple versions, an original and others). Same for people
mentioned, quotes, etc. 

3. explain the relevance of a quote.

4. transition between paragraphs, quotes etc. is key. similar to the rule
   above, make sure the paragraphs are transitioning from one point to another. 

5. avoid jargon. if a technical/academic/etc term needs to be used, maybe it
   needs its own page as well. (i.e.  "it refers to a very specific kind of
     'digital', a highly technoprogressivist and industry-defined kind that
     became prominent in the 'digital revolution' hype of the 1990s."
     technoprogressivist is not a common word that could be understood easily,
     either provide a short explanation or make a page for it)

6. check if there are other pages you can crosslink in your text (i.e. you
   mention hardware, link the hardware page there)

7. avoid using brackets (as much as possible), it breaks the reading (and
   comprehending) flow (quite a bit) (right?).

8. avoid passive-aggresive language. rather, explain why some concepts are
   wrong/didn't work/are bad. (i.e. "digital revolution" hype of the 1990s. why
       was it a hype?)

9. make sure all formatting choices are unified in your text. (i.e.
   https://permacomputing.net/Principles/ here, the sub-principles are written
   as a paragraph first, and then as list. should be the same style). 

10. use formatting sparingly and with purpose.

11. avoid repetation and redundant words, be concise, simple, clear.

12. don't forget to save! :D


On attribution, quotes and footnotes
------------------------------------

At the current stage of maturity of this wiki, it is often not advisable to
write a comprehensive articles about a topic if someone else has already done
it elsewhere. Put in a link to that external resource instead. (In the future,
we will perhaps want to host copies of all these "dependencies" in the local
repository as well, but not yet.)

When introducing a new term, please try to include a proper and non-biased
definition of the topic before proceeding to the permacomputing-specific points
of view. You can use [[Wikipedia]] or other sources for this, **just make sure
you properly attribute and quote** (we are CC0, Wikipedia is CC-BY-SA, so you
can't just copy-and-paste even from there).

If you rely on other sources for the writing of a section, do not be lazy or
mindlessly copypaste from other sources. In general, **do not invisibilize
people from which you took inspiration and/or learned something from**. Take
the opportunity of contributing to this wiki to also point to their work and
research. You must properly attribute your source. You have 4 options:

* **Hyperlinks:** Sometimes it's enough to point to external reference as an hyperlink if there is not much to discuss. For instance [Ursula K. Le Guin has an interesting take on technology](http://www.ursulakleguinarchive.com/Note-Technology.html).
* **Footnotes:** Can be handy to drift a bit[^drift] but also to give a proper footnote reference when paraphrasing and referencing the thoughts of someone else. For instance Ursula K. Le Guin has been critical of a specific usage of the work *technology* when misused to only refer to the most recent developments, which also happen to be the most problematic[^leguinref].
* **Inline quotes:** Use this with footnotes when you want to quote something short inline. For instance Ursula K. Le Guin offers to understand technology more broadly as "the active human interface with the material world"[^leguinrefquote].
* **Block quotes:** Finally, you may want to quote entirely a part of someone else's writing, in which case, use the block quote formatting, with a footnote for the reference. For instance here is how Ursula K. Le Guin suggests to reconsider how we use the word *technology*:

> Technology is the active human interface with the material world.
> But the word is consistently misused to mean only the enormously complex and specialised technologies of the past few decades, supported by massive exploitation both of natural and human resources.
> This is not an acceptable use of the word. [^leguinrefquote2]

[^drift]: parenthesis could be used as well, sure, or long — em dashes, but if you're going to fork the discussion to something that's too long to fit in the flow of the main text, and that does not need its own page, then a footnote can be quite handy.
[^leguinref]: See Ursula K. Le Guin, "A Rant About 'Technology'," 2004, [http://www.ursulakleguinarchive.com/Note-Technology.html](http://www.ursulakleguinarchive.com/Note-Technology.html).
[^leguinrefquote]: Ursula K. Le Guin, "A Rant About 'Technology'," 2004, [http://www.ursulakleguinarchive.com/Note-Technology.html](http://www.ursulakleguinarchive.com/Note-Technology.html).
[^leguinrefquote2]: Ursula K. Le Guin, "A Rant About 'Technology'," 2004, [http://www.ursulakleguinarchive.com/Note-Technology.html](http://www.ursulakleguinarchive.com/Note-Technology.html).


### Limitations of the footnotes

This it not biblatex/biber. So as you can see in the examples above, you cannot
reuse an existing footnote, and there is not elegant handling of repetition (no
Ibid.).


Reference style
---------------

When referencing, please use the [Notes and Bibliography version of The Chicago
Manual of
Style](https://www.chicagomanualofstyle.org/tools_citationguide/citation-guide-1.html).
However, this is not an academic paper, don't overthink it or spend ages on it,
try to make it work as best as you can, it's just to have some overall
consistency. No sweat :)


Acceptable content
------------------

Basically any topic is allowed as long as it can be discussed from a
permacomputing-relevant point of view and do not break the [[terms]].


Licensing
---------

While editing the Permacomputing wiki, you agree that your contribution will be
published and made available under the [CC0
Waiver](https://creativecommons.org/publicdomain/zero/1.0/legalcode-plain). If
you use images, photos, from other sources, please make sure to give full
credit and if available the license/tersm under which the image is made
available.
