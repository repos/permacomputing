**Collapse OS** is a [[Forth]]-based [[operating system]] and a collection of tools and documentation with a single purpose: preserve the ability to program microcontrollers through civilizational [[collapse|Collapse computing]]. It is designed to:

* Run on minimal and improvised machines.
* Interface through improvised means (serial, keyboard, display).
* Edit text and binary contents.
* Compile assembler source for a wide range of MCUs and CPUs.
* Read and write from a wide range of storage devices.
* Assemble itself and deploy to another machine.

Collapse OS was also [ported](https://github.com/schierlm/collapseos-uxn) to the [[UXN]] virtual machine.

[[Dusk OS]] will be a similar OS geared at more powerful devices and aimed at general-purpose post-collapse uses.

See also:

* [The official website of Collapse OS](http://collapseos.org/)
* [[Collapse computing]]
* [[Civboot]]
* [[Dusk OS]]
