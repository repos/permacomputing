**Wirth's law** is [[Niklaus Wirth]]'s variant of the [[Jevons paradox]]: Software is getting slower more rapidly than hardware is getting faster.

Wirth presented this idea in 1995 in an article as a means to justify the need for "lean software", something like Wirth's own [[Oberon]] system.

For future scenarios where hardware stops getting faster and may actually start getting smaller and simpler, we may envision an inversion of Wirth's law: software will be getting better more rapidly than hardware will be getting smaller.
