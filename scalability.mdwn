Scalability
===========

Scalability usually means upscalability: the property of a system to handle
a growing amount of work by adding resources to the system.

In permacomputing, downscalability may be more important than upscalability.
Systems in general are supposed to remain small, so they don't need to be
designed with upscalability in mind. However, there may often be a need to
implement an idea or an algorithm in a very restricted environments, which
creates preference for downscalable ideas and algorithms.
