Notes regarding `eik`

http certs
----------

We leave the visitor the option to `http` or `https`. This is done in `nginx` with only the following instructions in each `site-enabled` config:

    listen 80;
    listen 443 ssl;

However when authentication is required we force `https` like this:

    location /supercoolauth.cgi {
        return 301 https://$http_host$request_uri;
    }

We use `acme.sh` for handling certs, however expanding certs with a new domain is still not supported. The workaround is to recreate a whole new cert with all the domains. For instance, as root:

    acme.sh --issue --nginx \
     -d eik.permacomputing.net \
     -d top.permacomputing.net \
     -d bitrot.permacomputing.net \
     -d git.permacomputing.net \
     -d compost.permacomputing.net \
     -d berlin.permacomputing.net \
     -d newdomain.permacomputing.net
    
    acme.sh --install-cert \
    -d eik.permacomputing.net \
    -d top.permacomputing.net \
    -d bitrot.permacomputing.net \
    -d git.permacomputing.net \
    -d compost.permacomputing.net \
    -d berlin.permacomputing.net \
    -d newdomain.permacomputing.net \
    --key-file /etc/nginx/certs/key.pem \
    --fullchain-file /etc/nginx/certs/cert.pem \
    --reloadcmd "systemctl force-reload nginx"

Our `acme.sh` makes use of letsencrypt CA servers, as the default one from ZeroSSL keeps on being fussy. The setting was changed like this:

    acme.sh --set-default-ca --server letsencrypt

Removing a cert is a slightly trickier. We think [this](https://community.letsencrypt.org/t/domain-is-not-a-issued-domain-skip/132337/5) might work. You edit a file in the `~/.acme.sh` by hand 😬

cerca
-----

We maintain a single patch on `bbs-patches` to disable registration (See below for how we manage this with our own Git hosting).

Our system configuration documentation has been upstreamed to [`docs/hosting.md`](https://github.com/cblgh/cerca/blob/main/docs/hosting.md) 🎉

You can edit the CSS, about pages, registration instructions etc. all in `/var/www/bitrot.permacomputing.net/content/`. There are some other configurations lying around there which might be useful also. When making changes, you need to restart Cerca: `systemctl restart cerca`.

If you run into issues, have ideas for improvements or otherwise wanna get in touch with the main developer, cblgh, they're very open to discussions on the issue tracker [here](https://github.com/cblgh/cerca/issues).

### Hacking cerca

You can push/pull changes from our private temporary branch. You need to add the `eik` repository to your local checkout. You can do this with something like so: `git remote add pmc ssh://foo@eik:/var/www/git.permacomputing.net/repos/cerca.git` (see "git hosting" below for more details). Then you can `git fetch --all pmc` and you should have a copy of the `bbs-patches` branch. Push all your hacks there so we can get lost in hacks together 🤓

### Update cerca

Due to resource constraints on `eik`, we are building `cerca` on our own machines and then uploading & replacing the binary to update a new version.

You can build a static binary on your own machine with:

    go build -v \
      -ldflags="-s -w -linkmode 'external' -extldflags '-static'" \
      ./cmd/cerca

### warawara 🤖

When there is activity on the forum we get XMPP notifications from [warawara](https://git.permacomputing.net/warawara). The binary is located at `/usr/local/bin/warawara` and if it needs to be restarted, you can `systemctl restart warawara`.

### compost 👩‍🌾

Our own [image uploader](https://git.vvvvvvaria.org/pmc/image_upload) for bitrot. It's deployed on eik, `systemctl status compost` to learn more.

motd message
------------

See `/etc/update-motd.d/` for the scripts. When a user logs in, these scripts are all run. They must be executable to be run. Feel free to add some nice welcome screens!

There are also some configuration knobs in `/etc/ssh/sshd_config` which related to showing the last log in and the default `/etc/motd`. These have been turned off for now.

git hosting
-----------

Git repositories are located in `/var/www/git.permacomputing.net/repos`. Any user in the `gitusers` group has access to reading and writing these files.

*  to add a new eik user to the group (replacing `USERNAME` as needed): 

   ```usermod --append --groups gitusers USERNAME```

*  to create a new repository on eik (replacing `USERNAME` and `REPONAME`): 

   ```
   ssh USERNAME@eik
   cd /var/www/git.permacomputing.net
   ./new_repos.sh REPONAME "one line about the repos"
   ```

*  to clone (locally) your new repo:

   ```
   git config --global init.defaultBranch main  # if not done already
   git clone USERNAME@eik:/var/www/git.permacomputing.net/repos/REPONAME.git
   ```

*  to add a repository as a git remote (for `git push`, replacing `USERNAME` and `REPONAME`):

   ```
   git remote add pmc \
     ssh://USERNAME@eik:/var/www/git.permacomputing.net/repos/REPONAME.git
   ```

stagit
------

[stagit](https://codemadness.org/stagit.html) is used to produce the staticly generated web interface that is [git.permacomputing.net](https://git.permacomputing.net).

If you follow the instructions above ("git hosting") to add a new git repository, then all the needed hooks and git repo modifications have been installed. Next time you push, you should see your changes reflected on [git.permacomputing.net](https://git.permacomputing.net) and [https://git.permacomputing.net/REPO](git.permacomputing.net/REPO).

home pages
----------

You can share web stuff via a local `public_html` directory.

    cd
    mkdir public_html
    echo "hello, world! > public_html/index.html

Then visit `eik.permacomputing.net/~YOURUSERNAME`.

The `nginx` incantation for this is configured in `/etc/nginx/sites-enabled/eik.permacomputing.net`.

mosh
----

[mosh](https://mosh.org) is installed on the server to make doing terminal ops more convenient on intermittent connections, long running commands and so on. See the website for more information. You simply replace `ssh` with `mosh` when you want to connect to the server. You need to install the `mosh` client on your machine also (e.g. `apt install mosh`).

firewall
--------

[ufw](https://help.ubuntu.com/community/UFW) is installed as a "frontend" for `iptables`. Defaults are set to allow outgoing and block incoming traffic. Only specific ports are open and allow incoming.
