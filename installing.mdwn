These install notes make the following assumptions: Debian OS, nginx, fcgi-wrap, stagit. Also these are notes, not all the steps are provided, such as restarting nginx, enabling https, etc.

## Base installation

    sudo apt install ikiwiki
    sudo mkdir /var/www/damaged.bleu255.com-src /var/www/damaged.bleu255.com
    sudo chown $USER:$USER /var/www/damaged.bleu255.com*
    echo "hello" > /var/www/damaged.bleu255.com-src/index.mdwn
    mkdir ~/ikiwiki-cfg
    ikiwiki --verbose /var/www/damaged.bleu255.com-src /var/www/damaged.bleu255.com --url=https://damaged.bleu255.com --dumpsetup ~/ikiwiki-cfg/damaged.setup
    ikiwiki --setup ~/ikiwiki-cfg/damaged.setup

## Configuration changes

IMPORTANT, any changes to the wiki configuration must be followed by this to reflect changes:

    ikiwiki --setup ~/ikiwiki-cfg/damaged.setup

## Enable CGI

    # ~/ikiwiki-cfg/damaged.setup
    cgiurl: 'https://damaged.bleu255.com/ikiwiki.cgi'
    cgi_wrapper: '/var/www/damaged.bleu255.com/ikiwiki.cgi'

    # /etc/nginx/sites-available/damages.bleu255.com
    server {
      listen 443;
    
      server_name damaged.bleu255.com;

      root /var/www/damaged.bleu255.com;
      index index.html index.htm;
      access_log /var/log/nginx/damaged.bleu255.com-access.log;
      error_log /var/log/nginx/damaged.bleu255.com-error.log;

      location / {
        try_files $uri $uri/ =404;
      }

      # Max size of file upload
      client_max_body_size 10m;

      location /ikiwiki.cgi {
        gzip off;
        fastcgi_pass unix:/var/run/fcgiwrap.socket;
        fastcgi_index ikiwiki.cgi;
        fastcgi_param SCRIPT_FILENAME /var/www/damaged.bleu255.com/ikiwiki.cgi;
        fastcgi_param DOCUMENT_ROOT /var/www/damaged.bleu255.com/;
        include /etc/nginx/fastcgi_params;
      }
    }

## Enable git

    ikiwiki-makerepo git /var/www/damaged.bleu255.com-src /var/www/git.bleu255.com/repos/damaged.bleu255.com.git

    # ~/ikiwiki-cfg/damaged.setup
    rcs: 'git'

    ikiwiki --changesetup ~/ikiwiki-cfg/damaged.setup

    # ~/ikiwiki-cfg/damaged.setup
    git_wrapper: /var/www/git.bleu255.com/repos/damaged.bleu255.com.git/hooks/post-update
    git_wrapper_background_command: git push

    ikiwiki --setup ~/ikiwiki-cfg/damaged.setup
    cd /var/www/damaged.bleu255.com-src
    git config pull.rebase false

## stagit hooks and stagit diff URLs

    cd /var/www/git.bleu255.com/repos/damaged.bleu255.com.git/hooks
    ln -s ../../../update_single.sh post-receive

    # ~/ikiwiki-cfg/damaged.setup
    diffurl: https://git.bleu255.com/damaged.bleu255.com/commit/\[\[sha1_commit\]\].html


## Limited web access

There are 2 ways to deal with user:pass for web access (if we rule out external auth like openid):

* outsource it to httpd-auth, implies that someone has to maintain the list of users and their passwd manually 
* or add a password prompt during account creation, something we'd share with only trusted people, or people showing interest, etc. We choose this approach

    # ~/ikiwiki-cfg/damaged.setup
    account_creation_password: pa55w0rd
    ikiwiki --setup ~/ikiwiki-cfg/damaged.setup

## Support for sending emails

Useful to reset passwords, etc.

    sudo apt install libmail-sendmail-perl
    
    # ~/ikiwiki-cfg/damaged.setup
    adminemail: yolo29383@hotmail.com

    ikiwiki --setup ~/ikiwiki-cfg/damaged.setup

## Stronger password hashes

    sudo apt install libauthen-passphrase-perl

    # ~/ikiwiki-cfg/damaged.setup
    password_cost: 16

## Disable OpenID and emailauth

    ikiwiki --setup ikiwiki-cfg/damaged.setup --disable-plugin openid
    ikiwiki --setup ikiwiki-cfg/damaged.setup --disable-plugin emailauth

    # ~/ikiwiki-cfg/damaged.setup
    disable_plugins: [emailauth, openid]

## Default git commit message when none provided

Ikiwiki and a recent enough version of git allow for empty git messages (the "Optional description of this change" while editing on the web), that's nice but it makes stagit history impossible to browse because it uses such messages as links. To make ikiwiki provide a default commit message when non given, you can do this:

    diff --git a/git.pm.old b/git.pm
    index 2bc2500..2198207 100644
    --- a/git.pm.old
    +++ b/git.pm
    @@ -680,21 +680,8 @@ sub rcs_commit_helper (@) {
            $params{message} = IkiWiki::possibly_foolish_untaint($params{message});
            my @opts;
            if ($params{message} !~ /\S/) {
    -               # Force git to allow empty commit messages.
    -               # (If this version of git supports it.)
    -               my ($version)=`git --version` =~ /git version (.*)/;
    -               if ($version ge "1.7.8") {
    -                       push @opts, "--allow-empty-message", "--no-edit";
    -               }
    -               if ($version ge "1.7.2") {
    -                       push @opts, "--allow-empty-message";
    -               }
    -               elsif ($version ge "1.5.4") {
    -                       push @opts, '--cleanup=verbatim';
    -               }
    -               else {
    -                       $params{message}.=".";
    -               }
    +               # Force a message to commit if none given.
    +               $params{message}.="empty web commit";
            }
            if (exists $params{file}) {
                    push @opts, '--', $params{file};


