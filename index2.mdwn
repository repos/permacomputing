Welcome to the Permacomputing wiki!

![permaflower](../pmclogo-neau.png)

What?
-----

Permacomputing is both a concept and a community of practice oriented around
issues of resilience and regenerativity in computer and network technology
inspired by permaculture.

In a time where computing epitomizes industrial waste, permacomputing
encourages the maximizing of hardware lifespans, minimizing energy use and
focusing on the use of already available computational resources. We do this we
want to find out how we can practice good relations with the Earth by learning
from ecological systems to leverage and re-center existing technologies and
practices. We are also interested in investigating what a permacomputing way of
life could be, and what sort of transformative computational culture and
aesthetics it could bring forward. 

The principles of permacomputing are: care for life, care for the chips,
keep it small, hope for the best, prepare for the worst, keep it flexible,
build on solid ground, amplify awareness, expose everything, respond to
changes, everything has a place

The properties of permacomputing works are:

* accessible: well documented and adaptable to an individual's needs.
* compatible: works on a variety of architectures.
* efficient: uses as little resource (power, memory, etc) as possible,
  minimization
* flexible: modular, portable, adapts to various use-cases.
* resilient: repairable, descent-friendly, offline-first and low-maintenance,
  designed for disassembly, planned for longetivity, planned longevity,
  lifespan maximization, designed for descent


Why?
----

To practice an alternative to the characteristics of the mainstream computing world.

The principles of the contemporary dominant computational culture are:
disregard for life, disregard for the chips, more is better, assume limitless
resources, keep it controlled, outsource the problem, amplify ignorance,
  obfuscate everything, destroy communities, achieve monopoly

The properties of such ICT industry are:

* inaccessible:greenwashing, Californian ideology, pseudosimplicity, otherness
* incompatible: vendor lock-in, proprietary
* inefficient: bloat, maximalism, cryptocurrency, calculation factory,
  cornucopianism
* rigid: monoculture, siliconization
* failing: silver bullet, planned obsolescence, wishcycling, software rot
* extractivist: attention economy, capitalism, Big Tech, neoliberalism


How?
----

How can I engage with permacomputing? Where to start, where to find practical
information?

* Starter's manual
* Projects
* Communities of Practice
* Courses and workshops
* Library







