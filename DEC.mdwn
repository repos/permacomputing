Damaged Earth Catalog
=====================

About
-----

The Damaged Earth Catalog is a growing online catalog of the different terms in circulation, used by communities of practice, in relation to computing and network infrastructure informed by ecological ethics, degrowth, resilience, repair, and minimalism.

It is currently developed by [[l03s]] as part of her PhD research. Entries on this page should point to the Damaged Earth Catalog wiki, check with [[l03s]] for suggesting new additions. Don't duplicate entries/work.

[[https://damaged.bleu255.com]]


Link to permacomputing
----------------------

At the moment permacomputing is moving from a set of interrelated ideas and practices to a more coherent system of thought, as best exemplified with its [[principles]]. It's a work in progress and different people have different views to it, and how to best approach its openness/closeness.

To be sure, many people and collectives have independently come up with similar ideas, so there's a lot of overlap between permacomputing and other concepts such as:

* [Collapse Informatics](https://damaged.bleu255.com/Collapse_Informatics/)
* [Computing within Limits](https://damaged.bleu255.com/Computing_within_Limits/)
* [Degrowth](https://damaged.bleu255.com/Degrowth/)
* [Frugal computing]
* [Salvage computing](https://damaged.bleu255.com/Salvage_Computing/)
* etc.

And looking further there are also ideas and movements that have some similarities but maybe are not as closely related:

* [Appropriate technology](https://damaged.bleu255.com/Appropriate_Technology/)
* [Begnin Computing](https://damaged.bleu255.com/Benign_Computing/)
* [Convivial computing](https://damaged.bleu255.com/Convivial_Computing/)
* [Feminist Technology](https://damaged.bleu255.com/Feminist_Technology/)
* [Green software engineering]
* [Heirloom computing]
* [Liberatory Technology](https://damaged.bleu255.com/Liberatory_Technology/)
* [Low-Tech](https://damaged.bleu255.com/Low-Tech/)
* [Permaprogramming]
* [Permatech]
* [Rustic computing]
* [Small Technology](https://damaged.bleu255.com/Small_Technology/)
* [Sustainable ICT]
* etc.

Is it more fruitful to think about all these ideas as different viewpoints to the same thing rather than independent "movements" whose borders need to be unambiguously defined? or on the contrary do they carry irreconcilable deviations and incompatible ideologies or world views?

This is the research and contextualisation that takes place at the Damaged Earth Catalog and help informs and bring perspectives on the constant becoming of permacomputing.

