A p-code machine is a [[virtual machine]] designed to execute the [[assembly language]] of a hypothetical CPU.

[[Niklaus Wirth]] specified a simple p-code machine in the 1976 book Algorithms + Data Structures = Programs. The machine had 3 registers - a program counter p, a base register b, and a top-of-stack register t. There were 8 instructions:

    lit 0, a  : load constant a
    opr 0, a  : execute operation a (13 operations: RETURN, 5 math functions, and 7 comparison functions)
    lod l, a  : load variable l,a
    sto l, a  : store variable l,a
    cal l, a  : call procedure a at level l
    int 0, a  : increment t-register by a
    jmp 0, a  : jump to a
    jpc 0, a  : jump conditional to a[6]

## Operations

    0: begin {return}
        t := b - 1; p := s[t + 3]; b := s[t + 2];
    end;
    1: s[t] := -s[t];
    2: begin t := t - 1; s[t] := s[t] + s[t + 1] end;
    3: begin t := t - 1; s[t] := s[t] - s[t + 1] end;
    4: begin t := t - 1; s[t] := s[t] * s[t + 1] end;
    5: begin t := t - 1; s[t] := s[t] div s[t + 1] end;
    6: s[t] := ord(odd(s[t]));
    8: begin t := t - 1; s[t] := ord(s[t] = s[t + 1]) end;
    9: begin t := t - 1; s[t] := ord(s[t] <> s[t + 1]) end;
    10: begin t := t - 1; s[t] := ord(s[t] < s[t + 1]) end;
    11: begin t := t - 1; s[t] := ord(s[t] >= s[t + 1]) end;
    12: begin t := t - 1; s[t] := ord(s[t] > s[t + 1]) end;
    13: begin t := t - 1; s[t] := ord(s[t] <= s[t + 1]) end;


See more:

* [On Wikipedia](https://en.wikipedia.org/wiki/P-code_machine)
