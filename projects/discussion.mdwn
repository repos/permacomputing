Editorial guideline:

listing:

- name of page/file should be the project name, followed by a oneliner

dedicated page:

- opening paragraph(s) should be a summary of the project
- "relevance to permacomputing", section dedicated to explain link to pmc
- "limits and challenges", section dedicated to constructive critique and potential points for improvements.
- "links", section dedicated to dump external links

Add internal/external hyperlinks for relevant keywording.

See [[Cerca]] page for a complete example.
