**Emotionally Durable Design** is a framework brought forward by Jonathan Chapman. It proposes attributes that should be considered in designing things. The aim is to enable us humans to form stronger bonds with the designed things.

The attributes brought forward in the Design-Nine paper are:

* Relationships
* Narratives
* Identity
* Imagination
* Conversations
* Consciousness
* Integrity
* Materiality
* Evolvability

Certain attributes are clearly coming from design, treating things as can be expected. Others, for example *Conversation* and *Consciousness*, are more philosophical. These attributes are treating things as non-human beings, essentially flattening the ontological hierarchy between humans and non-humans. This approach is common in indigenous epistemologies. There is no direct equivalent in western (EU- and US-centric) practice, but traces of it can be found in philosophy, for example in the book [[Vibrant Matter]] by Jane Bennett.

-

My, [[thgie]], personal opinion: The book by Jonathan Chapman is difficult at times, going into formulations that can be read as classist. I appreciated the practical/designerly approach to think about non-human ontologies.

## Bibliography

Chapman, J. (2015). [Emotionally durable design: Objects, experiences and empathy.](https://www.routledge.com/Emotionally-Durable-Design-Objects-Experiences-and-Empathy/Chapman/p/book/9780415732154)

Haines-Gadd, M., Chapman, J., Lloyd, P., Mason, J., & Aliakseyeu, D. (2018). Emotional Durability Design Nine—A Tool for Product Longevity. Sustainability, 10(6), 1948. [https://doi.org/10.3390/su10061948](https://doi.org/10.3390/su10061948)
