**Bloat** refers to something wastefully and unnecessarily large. In software, it may refer to the presence of program code that is perceived as unnecessarily long, slow, or otherwise wasteful of resources, but a considerable amount of bloat originates from external [[dependencies|dependency]].

One formulation of bloat is [[Wirth's law]], a variant of [[Jevons paradox]]: software is getting slower more rapidly than hardware is getting faster.
