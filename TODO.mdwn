To keep track of things [[TODO]] before launching the wiki:

Server
------

* migrate pmc wiki to new server
* modify cron zip script to run only if changes made to wiki

ikiwiki
-------

* strategy for handling images / decide on policy for images (ditherpunk?)
* look into the file upload plugin for images and stuff
* tag index plugin
* A more wikiwiki layout (top and bottom menu)


Content
-------

* section for curriculum postdoc thing (wip)
* section on workshop outline "The Cloud is Just My Old Computer"
* new properties structure from katía/crunk (wip, will be spread over pmc long page and the issues with tech page)
* laypeople-accessible introduction to permacomputing
* switch to index3
