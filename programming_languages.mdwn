When assessing programming languages, we should pay attention to:

* How complex is the language? How long would it take to learn all the syntactical details? How long would it take to implement a compiler/interpreter from scratch?
* How mature is the language? Do changes to the specification often break backwards compatibility? How much hacking does it require to compile and run a decades-old program in current implementations of the language?
* Are there several alternative implementations of the language? (It is generally a good sign if there are)
* What are the [[software]] characteristics of these implementations (e.g. disk and memory footprint and compilation speed)? How much resources would it take to [[bootstrap|bootstrapping]] them?
* What kind of platforms do these implementations target? Is it possible to port a program to a very small and/or obscure device without switching to another language?
* How fast and compact is the generated code? What are the overheads and mandatory dependencies like? Does the hello world require bytes, kilobytes, megabytes or gigabytes of memory if all the dependencies are included?

Asking "what is the most suitable programming language for permacomputing?" is akin to asking what is "the most suitable plant for permaculture". The entire question contradicts itself. 

There is a high diversity of possible tasks and programs, and different programming languages suit them in different ways. Not all software needs to last for decades, run efficiently or be ultra-secure. However, it is still good if the language does not prevent this.
                      
## programming languages
                 
  * [[C]]
  * [[Forth]]
  * [[Lisp]]
  * [[Lua]]
  * [[Nim]]
  * [[Hare]]
  * [[Zig]]
  * [[Smalltalk]], see [The Cuneiform Paper](http://www.vpri.org/pdf/tr2015004_cuneiform.pdf)
  * [[Go]]

See also:

  * [Drew DeVault's blog post about benchmarking compilers by Hello world size](https://drewdevault.com/2020/01/04/Slow.html)
  * [Blog post about a research in energy efficiency of programming languages](https://www.sendung.de/2022-07-24/programming-languages-energy-efficiency/)
