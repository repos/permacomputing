**C** is a general-purpose programming language created in the 1970s for the system programming needs of the [[Unix]] operating system.

The main benefit of C is that it is ubiquitous and quite mature. There are C compilers for nearly any imaginable processor architecture, and relatively old code often compiles quite well.

Compiled C code is generally quite resource-efficient. The speeds of compiled languages are often compared to C.

There are also languages whose compilers can produce C code to be compiled by a C compiler. These languages thus benefit from the optimization features and platform support of the C compilers:

  * [[Nim]]
  * [[V]]

Interpreted languages implemented in C:

  * [[Lua]]
  * [[Perl]]
  * [[Ruby]]
  * [[Python]]

As a language, C has many problems that subsequent languages have tried to fix with varying degrees of success. Examples of such languages:

  * [[C++]]
  * [[Objective-C]]
  * [[D]]
  * [[Go]]
  * [[Rust]]
  * [[Nim]]

Compilers:

  * GCC
  * Clang
  * [[Zig]] has a C/C++ compiler that produces much smaller binaries (even static ones) than the mainstream GCC and Clang toolchains.
  * [Open Watcom](http://www.openwatcom.org/) is a C/C++/Fortran compiler usable for targeting legacy x86 operating systems such as [[DOS]].
  * [Tiny C Compiler](https://bellard.org/tcc/) is a small (100+ KB) standalone C compiler for "modern" x86 and ARM targets (i.e. [[Linux]] but not [[DOS]]).
  * [vbcc](http://www.compilers.de/vbcc.html) is an optimizing C99 compiler particularly suitable for some legacy targets such as [[68000]] and [[6502]].
  * [cproc](https://sr.ht/~mcf/cproc) and [other compilers](https://c9x.me/compile/users.html) based on [the QBE compiler backend](https://c9x.me/compile).
