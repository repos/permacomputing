**Microform** is a miniaturized version of a document used for
[[non-digital]] archival purposes. The most common formats are **microfilm**
reels, **microfiche** sheets and **aperture cards** (punched cards that
contain a piece of microfilm).

Compared to digital storage, microforms have the following advantages and
disadvantages:

  * Longevity. In proprer conditions, a microfiche may last ~500 years.
  * Simplicity. Microforms are simply miniaturized photographs of readable documents, so viewing them only requires optical magnification technology. The obsolescence problems of digital formats are nonexistent.
  * Lossy copies. Like in many non-digital formats, some information is lost every time a copy is made, reducing the image quality.

A lot of early history of computing overlaps with that of microforms.
Visionaries like [[Paul Otlet]] envisioned things like "a library that fits
in a suitcase" storing thousands of books and a microform viewer in a small
space. Also, "Memex", the influential 1945 idea from Vannevar Bush, was
presented as an application of microform technology. [[Emanuel Goldberg]]
produced the first aperture card system, i.e. a microfiche storage system
with an automatic search.
