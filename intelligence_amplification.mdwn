Computers (and many of their predecessors such as mechanical calculators and
tabulating machines) were invented to assist humans in cognitive tasks such
as calculation and data processing. **Intelligence amplification** takes
place when computer interaction assists the human user in conceptual
thinking by e.g. improving access to information. A lot of today's common
computer applications (including the [[WWW]]) can be regarded as IA, even
though the concept became unfashionable in the 1980s.

For permacomputing purposes, IA can be regarded as a subset of
[[awareness amplification]].

The use of punched cards and mechanical devices for "enhancing natural
intelligence" was already suggested in 1832 by Semyon Korsakov and in the
1910s by Wilhem Ostwald. In these suggestions, each punch card would contain
an idea or a "micro-thought", and a mechanical device would assist in
finding and connecting them. Emanuel Goldberg (1930) and Vannevar Bush
(1945) combined this concept with a microfiche viewer/searcher.

In Douglas Engelbart's Augmentation Research Center, these ideas evolved
into a user interface prototype that supported a form of human-computer
symbiosis. Hypertext and many modern GUI concepts originate from Engelbart's
project.
