An **integrated circuit** (**IC**) or a **microchip** is a set of electronic circuits (usually consisting of MOSFET transistors) on a small flat piece of semiconductor material (silicon). It is currently the dominant technological basis for computers.

**[[Moore's law]]** and **Rock's law** have succesfully described the
history of semiconductor integration for decades: while it has been possible
to double the transistor density every two years, the cost of building a
state-of-art fabrication plant has doubled every four years. This has
[[centralized|centralized]] IC fabrication and distanced it from local and
small-scale production.

Types of ICs:

  * [[processor]]
  * [[memory]]
  * [[ASIC]] (nearly any complex IC that's not general-purpose can be called "application-specific")
  * [[logic chip]]s (e.g. the 4000 series and the 7400 series)
  * [[TTL]] 
  * [[FPGA]]
  * ...

Making
------

There are hobbyists who have succesfully produced ICs in garage-like conditions. In 2021, Sam Zeloof produced twelve 1200-transistor chips at home (the first microprocessor had 2300 transistors), although some second-hand special purpose equipment such as a maskless photolithography stepper were used.

DIY project information:

  * [Sam Zeloof](http://sam.zeloof.xyz/category/semiconductor/)
  * [HomeCMOS project](https://github.com/homecmos/homecmos-wiki)

The materials that end up in the finished products don't have
to be exotic or destructive (e.g. silicon, oxygen and aluminum
for the bare chip, and plastic/ceramic and some metal for
encapsulation). However, fabrication processes usually use
dangerous substances such as hydrogen fluoride (HF) to keep the
chip clean. Trichloroethylene (TCE) has poisoned groundwater
supplies several times, including in 1974 when a leak took
place in the MOS Technology Pennsylvania plant. Finding alternatives to these substances may be more urgent than developing local and small-scale production.

Zeloof has been able to eliminate some aggressive chemicals such as sulfuric acid from the process. HF is still mentioned, but CF4 and CHF3 are listed as alternatives for it.
