Welcome to the Permacomputing wiki!

![permaflower](../pmclogo-neau.png)

Permacomputing is both a concept and a community of practice
oriented around issues of resilience and regenerativity in computer and
network technology inspired by permaculture. ପໄଓ☾☼✫ -☆:*´

There are huge environmental and societal issues in today's computing,
and permacomputing specifically wants to challenge them in the same way as
permaculture has challenged industrial agriculture. With that said, permacomputing is
an anti-capitalist political project. It is driven by several strands of anarchism, 
decoloniality, intersectional feminism, post-marxism, degrowth, ecologism.

Permacomputing is also a utopian ideal that needs a lot of rethinking,
rebuilding and technical design work to put in practice. This is why a lot
of material on this wiki is highly technical.

Most importantly, there is no permacomputing kit to buy. See permacomputing as 
invitation to collectively and radically rethink computational culture. It is not a 
tech solution searching for a problem. You are free to start your own
initiative and use the term permacomputing, however please make sure you
understand the purpose and ethos of this project :)

For more information:

* [[A longer introduction|Permacomputing]]
* [[Principles]] for people making things
* [[Issues]] in today's computing

Resources:

* [[Getting Started]] - a starter's manual of some sort
* [[Library]] - texts and media about and around permacomputing
* [[Projects]] - selection of projects affiliated with or adjacent to permacomputing
* [[Assessments]] - review and analysis of existing and established pieces of technology

Network:

* [[Community]] - meet, work, organise and discuss with us!
