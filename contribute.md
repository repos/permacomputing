Access
======

If you feel like you belong here, you are very welcome to contribute to this
wiki!

You can [[contact|about]] us by email for an account. Please:

* Present yourself briefly;
* Include URLs to work/project/socials/etc;
* Explain your interest in permacomputing and tell us what/why/how you would
like to contribute to the wiki;
* Confirm that you have read the [[editing guideline|editing]] and agree to our
[[terms]].

Please cover these four points, we won't reply to vague or incomplete
requests. Really, we won't. It may take a few days for us to get back to you.

