The flower graphics should probably be hosted locally, I've only put it there so it could be transferred over. - [[@neau|neau]]

![permaflower.png](https://wiki.xxiivv.com/media/generic/permacomputing.png)

* Thanks, I've made it local. By the way, the PNG is in truecolor colorspace, making it paletted made it much smaller. [[viznut]]

Ah yes! Good call, it was originally cyan, I totally forgot about the color profile. - [[@neau|neau]]
