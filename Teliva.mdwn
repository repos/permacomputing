[**Teliva**](https://github.com/akkartik/teliva) is a text-mode platform for disseminating [[sandboxed|sandboxing]] applications. In this respect it resembles a web browser (without markup or a DOM for documents). The sandboxing model is more flexible than [[web browsers]]. 

## Relevance to permacomputing

It tries to nudge computer owners to think about what permissions they should grant untrusted applications, and to learn a little bit of programming to do so. It also encourages owners to look inside application code and provides a trivial edit-run debug cycle to help them make changes to untrusted applications.

See also:

* [A talk on Teliva](https://archive.org/details/akkartik-2022-01-16-fosdem)
