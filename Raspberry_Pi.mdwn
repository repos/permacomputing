**Raspberry Pi** is a popular family of inexpensive [[single-board computer]]s.
They are often used in electronics/computing hobbyist projects including
ones whose goals somewhat align with those of permacomputing.

While the Raspberry Pi is often among the best alternatives for many
purposes, a particular problem with it is that it is designed for
disposability rather than longevity. The cheapness and smallness of the
boards may also be deceptive and make it too appealing to purchase new ones.

## Chips

Most of the models are based on Broadcom SoC chips that are insufficiently
documented, even though the large and active user base somewhat compensates
on the problem of closed hardware.

An exception is the Pico whose microcontroller chip (RP2040) was designed by
the foundation itself and has apparently a rather complete register-level
documentation. An interesting feature of RP2040 is its programmable IO (PIO)
that is general-purpose rather than tied to specific protocols and
interfaces, while being powerful enough to e.g. generate video signals.

## Alternatives

There are many different single-board computers, some of which based on
fully documented or even open-source hardware. See [[single-board computer]].

