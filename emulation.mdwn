**Emulation** is a [[digital preservation]] technique involving mimicking the behavior of older hardware with software, tricking old programs into thinking they are running on their original platforms.

See also:

* [[bedrock platform]]
* [[virtual machine]] 
