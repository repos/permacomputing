**Collapse computing** or **Collapse informatics** is the study, design, and development of sociotechnical systems in the abundant present for use in a future of scarcity. Civilizational or technological collapse is an extreme example of such a future. The term "Collapse informatics" was coined by Bill Tomlinson in 2013.

A major project in Collapse computing is [[Collapse OS]], an operating system and set of tools for restarting computer technology after collapse.

Many [[Computing within Limits]] papers from the early years are about Collapse informatics.

While many may not agree with the collapse scenario or even a future of scarcity, the practical results of collapse computing are in line with the goals of permacomputing. Studies of the longevity of hardware components are relevant to [[lifespan maximization]] even without a collapse. Collapse computing is also relevant to resilience, self-sufficiency and the simplification of technology.

See also:

* [Tomlinson's 2013 article](https://www.researchgate.net/publication/262276832_Collapse_Informatics_and_Practice_Theory_Method_and_Design) DOI:10.1145/2493431
* [Unplanned Obsolescence: Hardware and Software After Collapse (PDF, LIMITS 2017)](https://kurti.sh/pubs/unplanned_limits17.pdf) DOI:10.1145/1235
* [Collapse computing at XXIIVV wiki](https://wiki.xxiivv.com/site/collapse_computing.html)
