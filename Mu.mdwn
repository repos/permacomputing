[**Mu**](https://github.com/akkartik/mu) is a minimalist stack of languages built up from machine code. It requires a processor from the x86 family, and builds up to a memory-safe language without any additional dependencies (though it can also be bootstrapped from a C implementation).

Like [[Forth]], Mu was intended to enable people to build interesting programs while being able to hold the entire system in their head. Unlike [[Forth]], Mu does so while being well-typed and providing good guardrails that make programs easier to debug. 2/3rds of the LoC are devoted to automated tests.

Performance is not a priority; Mu achieves its goals by focusing on very basic hardware support. It only supports one screen resolution (1024x768 with 256 colors) and has only one font (albeit with extensive Unicode support). There's just one device driver for hard disk storage: ATA disks in the inefficient PIO mode.

Mu is different from more retro computers like [[Uxn]] in assuming lots of RAM and disk. It uses a 32-bit address space and avoids space-saving tricks in favor of straightforward code.

Mu is currently dormant. You can create graphical programs that run without an external OS (albeit only emulated on Qemu so far). You can also create text-mode programs for Unix-descended systems. It's currently stalled in need of expertise debugging real hardware and implementing networking. The mouse driver also needs work.

Since Mu depends on no features of x86 newer than SSE, it runs on any x86 processor built in the 21st century. Programs built on Mu are also expected to be resistant to bitrot since they require fairly basic emulation capabilities. It currently serves as a time capsule to test these hypotheses.

See also:

* [An academic paper about Mu](http://akkartik.name/akkartik-convivial-20200607.pdf)
* [A summary of the Mu compiler on a single page.](http://akkartik.github.io/mu/html/mu_instructions.html) It's built in machine code so [needs to be really simple](https://github.com/akkartik/mu/blob/main/linux/stats.txt).
