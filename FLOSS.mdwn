# FLOSS

Free Libre and Open Source Software (FLOSS) is an umbrella acronym used to refer to software development practices in which the circulation of software source code is enabled by licensing strategies that promote and simplify the re-use of the code that would otherwise be limited by the author driven doctrine of copyright laws.

The two major definitions of FLOSS are free software and open source software. They almost entirely overlap and follow the same principle of providing a definition and a set of approved licenses that match this definition. These licenses can be either compatible or incompatible with each others, making composite projects either very simple, or very complicated. They can however be split into two large families:

* (strong, weak) copyleft licenses. Such licenses impose the person making modification to copyleft material to share their modification under the same condition/license than the code they modified. The idea is to promote circulation and virality;
* permissive or copyfree/copycentre licenses. Such licenses have much more simple conditions for reuse, if any, making possible to use such source for closed source software and proprietary systems.

While a popular method for software production and distribution, FLOSS has been increasingly scrutinised for its underlying liberal, possibly ultra-liberal ideology that has been more useful to the for-profit software industry, than it has been useful to foster the much anticipated digital commons of public interest, as envisioned in the late 90s and 00s. This is because both free software and open source software proponents support the idea of permitting the (re)use of FLOSS source code for *any* purpose. As a result a growing number of [[post-free culture]] licenses have started to emerge in the late 10s and early 20s to address issues of ethics and exploitation found in the *for any purpose* take of FLOSS. 
