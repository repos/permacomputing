Permacomputing is also learning to deal with existing technologies, often trying to find the least evil among bad alternatives. In [[hardware]] we can't afford being too picky because we'll want to lengthen the lifespans of already existing pieces of hardware (even bad ones), but in [[software]] we usually have more choice.

When assessing software and hardware, we'll want to focus on technicalities such as resource use (especially [[energy use]]), [[dependencies|dependency]] and [[documentation]] – mostly because many pieces of today's technology fail miserably in these areas.

See also:

* [[Hardware]]
  * [[Computers]]
  * [[single-board computer]]s
  * [[Microcontrollers]]
  * [[Peripherals]]
* [[Software]]
  * [[Programming languages]]
  * [[Operating systems]]
  * [[Protocols]]
  * [[File formats]]
