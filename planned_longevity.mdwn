**Planned longevity** is the opposite of planned [[obsolescence]]: the way
of designing systems, especially [[hardware]], so that it supports
[[lifespan maximization]].

Planned longevity is something that should ideally take place in the
industry that produces the hardware. Sometimes, the shortcomings of the
industry can be compensated by changing the [[firmware]] of the system or
switching to a third-party [[software]] platform.

Chips should be designed open and flexible, so that they can be
reappropriated even for purposes they were never intended for. Complex chips
should have enough redundancy and bypass mechanisms to keep them working
even after some of their internals wear out. (In a multicore CPU, for
instance, many partially functioning cores could combine into one fully
functioning one.)

Concepts that support planned longevity:

  * [[Design for disassembly]]
  * [[Open hardware]]
  * [[Morseware]]
