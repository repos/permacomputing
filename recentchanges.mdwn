[[!if test="enabled(meta)" then="""
[[!meta title="RecentChanges"]]
"""]]

So this is also a wiki page...

Recent changes to this wiki:

[[!inline pages="internal(recentchanges/change_*) and !*/Discussion" 
template=recentchanges show=0]]
