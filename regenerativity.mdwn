##Regenerative design, Regenerative computing

###The design of digital technologies (hardware and software) should be determined by democratic and participatory processes and help regenerate natural ecosystems and promote social cohesion. [^1]

Regenerativity principles disrupt and surpass technology design associated with power accumulation, a polarisation of opportunity and environmental inequalities, consumption and profit maximalisation.


- Regenerate natural ecosystems and social cohesion.
- Democratic, co-creative and sustainable.
- Creative problem solving with wide perspective and planet encompassing focus with humans as integral part.
- Evolution from designing objects to designing material flows and systems serving the common good.
- Design process should be as open, participatory and transparent as possible.
- Integrate diverse views, needs and issues (not just those of predominantly highly-educated, middle-class males in urban centres), co-design principles with active participation from all users are essential.



See also: 

Permacomputing [[research fields and methods]];



[^1]: [Digital Reset Report, TU Berlin, 2022](https://digitalization-for-sustainability.com/digital-reset/);
