**Solar Protocol** is a project that connects solar-powered [[WWW]] servers from around the globe, redirecting traffic to a server that has solar power available (i.e. the part of the globe where the sun is shining).

A problem with Solar Protocol is that it either neglects the energy requirements of the [[Internet]] infrastructure that lies between the solar-powered servers and the users, or takes them as a constant that only depends on the amount of transferred data. However, we can be fairly sure that routing a packet across the world takes much more energy than routing it across a country.

If the power consumption of a server is small to begin with, its "greenness" may very well get negated by a bad routing decision. It may very well be "greener" to just route a user to a nearby fossil-powered data center than to a solar-powered server on the other side of the world.

Taking the network into the equation is difficult because even academic estimations of the power consumption of Internet routing have varied by several orders of magnitude. Still, there seem to be no mentions of this issue on the Solar Protocol website, even though it discusses the energy consumption in the server side and the browser side. Given the prominence of SP in the media, it is highly unlikely that the people involved have not heard about this kind of critique.

In 2015, it was estimated in a meta-analysis by Aslan&al. that moving a gigabyte across a national cable network took 0.06 kWh. This is the same figure as for running a 10-watt server for 6 hours. Even if the current figures are much lower, they cannot be ignored as irrelevant if the server network actually serves data instead of mostly remaining idle.

From the permacomputing point of view, Solar Protocol has a lot of the right spirit and may work very well as an educational or artistic project. Unfortunately, the project will remain somewhat problematic until it considers the effect of the Internet infrastructure.

See also:

  * [Solar Protocol website](http://solarprotocol.net/)
  * [Why do estimates for internet energy consumption vary so drastically?](https://www.wholegraindigital.com/blog/website-energy-consumption/)
