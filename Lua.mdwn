[**Lua**](https://www.lua.org) is a general-purpose high-level language created since the 1990s at [PUC Rio de Janeiro](https://en.wikipedia.org/wiki/Pontifical_Catholic_University_of_Rio_de_Janeiro).

Lua provides a sweet spot along three axes:

* high-level dynamic syntax with anonymous functions and closures
* reasonably high performance by compiling to a virtual machine's bytecode
* small and approachable implementation with no dependencies beyond C

Lua also provides a framework for graphical applications called [LÖVE](https://love2d.org). LÖVE depends on the host OS for graphics. On Unix-descended systems, that is approximately the [SDL2](https://www.libsdl.org) library.
