**Aesthetics** is relevant to many aspects of computing. Here, we are mostly
concerned of the superficial visual appearances and their technological
bases.

A predomidant aesthetic in mainstream computing is **[[maximalism]]**, that
is based on the idea that increased detail, complexity and "fidelity" are
the key to "better graphics". This kind of preference is highly problematic
from the permacomputing point of view because it creates a preference for
energy usage maximization (when uncapped). Permacomputing should therefore
strongly prioritize non-maximalist aesthetic approaches.

The dominant approach on the [[demoscene]] is optimalism, or "capped
maximalism". It often proves that mass appeal can be reached despite tight
limitations, but the aesthetic basis is still maximalist – fitting a maximal
amount of content within the limits, the more the better.

Ideally, the low complexity itself can be a source of beauty: things can
look good *because* of their smallness, not *despite* it. If this succeeds
really well, even the most mainstreamy viewer won't be longing for more
resolution or detail.

In user interfaces, the ideal of low complexity may easily lead to the now
fashionable oversimplification, where the internal details are hidden from
the user. This is not what we want. We should rather find ways to keep users
aware of what is going without overwhelming them with the information.

[[Media minimization]] techniques sometimes lead to styles such as
"ditherpunk" that require acquired taste and are still more likely to belong
to the "despite" category than the "because" category.

Another example of acquired taste is "Unix brutalism" that uses a lot of
monospaced fonts, program code and other elements typical of
[[character terminal]]s. It should be noted that despite its "hardcore
low-level vibes" it is often a suboptimal way of using display hardware.

The characteristics of [[electronic paper]] (slow update speeds, low
saturation, no flashing, bookiness) may be used as an antithesis for the
psychologically intensive mainstream computer aesthetics – regardless of
what kinds of screens are actually used. Elements may grow in rather than
scroll in (more like plants than cars). The semblance of printed media
alludes to a world that is slower and more thoughtful than the mainstream
Internet.

See also:

* [Permacomputing Aesthetics paper, 2023](https://doi.org/10.21428/bf6fb269.6690fc2e)
* [Slides from the presentation of the paper](http://low.fi/~viznut/limits-slides/)
