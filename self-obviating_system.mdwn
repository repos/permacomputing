A **self-obviating system** is a system which renders itself unnecessary in the long term.[^sos] It is in opposition to systems designed to maximise time spent in use ("user engagement strategies"), as well as [[planned obsolescence|obsolescence]], as a self-obviating system does not require replacement by a system of the same type. It also contrasts to patterns described by [[Jevons_paradox]] or [[Wirth__39__s_law]] where new efficiencies actually result in more resource usage rather than less. The concept of **self-obviating systems** is central to the [[principles]] of permacomputing as a means to evaluate and build computing systems which are designed to assist in the flourishing of humans and the biosphere, promote community and inclusion, minimize resource use, and optimize the lifespans of computing devices by using them less and becoming self-sufficient and sustainable where possible.

Educational systems are frequently self-obviating as once the material is learned, the reference materials and activities are no longer needed.[^education]

## System design assessment questions

* What are the impacts of this project in the longer term? What could its impacts be on time spent using computers or the relationships between people who come in contact with this system?

* Can this system be repurposed in any way once our goals are accomplished? Will it be useful as-is to others, or in constituent parts if [[designed for disassembly|design for disassembly]]?

* How can this project obviate itself? Will it happen at once when a goal is reached, or after a particular amount of time, or using milestones?

* How can we evaluate if system use is increasing over time, demonstrating a failure to create self-sufficiency?

[^sos]: See [Self-Obviating Systems and their Application to Sustainability](https://core.ac.uk/download/pdf/158298928.pdf), 2015, Tomlinson, et al.
[^education]: [Self-obviating Systems: Interview with Bill Tomlinson](http://postgrowth.art/self-obviating-systems-En.html)
