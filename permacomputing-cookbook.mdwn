## a cookbook for permacomputing ##

**!Nov.23: The project was discontinued due to insufficient resources and cooperation.**

.  .  .  .  
Bringing the idea of permacomputing to a broader audience would ideally use a "down-to-earth"-HowTo. This subproject aims to design and publish a "Cookbook for Permacomuting" that lists easy achievable concepts and guidelines for action in project form. 
Goals of the the book are:

* **easy to maintain**: by making it easy to keep content and documentation up to date
* **pleasant to read**: with a maximally large readership also and above all in education
* **easy to deploy**: the book should be maximally easy to distribute, also digitally


Person in charge is Claus Wilcke:  info [at} clauswilcke [dot} com

### What is it about? ###

The idea of this project is to publish a cookbook for permacomputing. It is thought as a kind of primer that introduces as many people as possible, ideally also school classes, to the topic of permacomputing. The idea of the book is divided into two parts: First and foremost, it should offer practical help, i.e. present realisable projects and concrete steps. At the same time, however, it should also explain the ideas of permacomputing, so that readers can ideally use these building blocks of practice and theory to open up new possibilities for action in the sense of permacomputing. 


### About this project: ###
This is a relatively young project, having started in May 2023. Currently, two main areas are being worked on: 

(1) The **editing and form of the publication**: We are thinking of a documentation in Markdown, which can then lead to an attractively laid out publication form. This will initially be digital, although a print product would be important, especially for classes and seminars. The transfer from content in Markdown to an appealing, print-friendly design should not be easy. We are open to suggestions on how this could be done (also jointly) and would be pleased to receive them. 


2) **Acquiring financial support**: We are currently searching the political map, that means above all the European and German Greens, for foundations or funding pots that would support such a cookbook. The amount of money would have to be about € 12K to support a small, part-time editorial team for about a year, who can take care of content, such as written contributions or pictures and graphics. 




## what has happened so far: ##
### November '32###
*The project was discontinued due to insufficient resources and cooperation

### July '23 ###
* Setting up this page
* [ first draft draft](https://arbeitsgruppen.clauswilcke.de/lib/exe/fetch.php?media=application-foundation-funds_cookbook-permacomputing.pdf/ "Application of foundation funds for the production of a 'Cookbook for Permacomputing'")  for a application of funding

### June '23  ###
* Contacting the European Greens in search of financial support/foundations
* Contacting the Greens in the German Bundestag in search of financial support/foundations


### May '23 __ ###
- initial idea, contact to the nice folk, maintaining this wiki ;-)
