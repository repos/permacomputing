**lines.love** is an editor for plain text where you can also draw simple line drawings.

## Relevance to permacomputing

* It does something useful to non-programmers. It's not just a research project, it's intended to be supported over the long term.
* It illustrates a graphics stack ([[C]],[[Lua]],[[LÖVE|Love2d]]) that runs on just about any computer or [[OS|operating systems]], is easy to compile, reasonably performant, and has fairly parsimonious [[dependencies|dependency]].
* It is designed to be easy to modify. It always ships with source code, it's easy to modify using the exact same tools used to run it, and the source code includes lots of automated tests to protect newcomers.

See also:

* [Official Website](http://akkartik.name/lines.html)
