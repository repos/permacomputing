**Smalltalk** is an object-oriented, dynamically typed reflective programming language. Kay says their Smalltalk [[virtual machine]] for the 8086 was 6 kilobytes of machine code.

* [ST-72 on the SqueakWiki](https://wiki.squeak.org/squeak/989)

## Relevance to permacomputing

While the language itself might not apply every aspect of its own design philosophy, the guidelines drafted out in [Design Principles Behind Smalltalk](https://www.cs.virginia.edu/~evans/cs655/readings/smalltalk.html), might in some way or other align with the values of Permacomputing.

* [[human-scale]]: If a system is to serve the creative spirit, it must be entirely comprehensible to a single individual.
* [[bedrock platform]]: A system should be built with a minimum set of unchangeable parts; those parts should be as general as possible; and all parts of the system should be held in a uniform framework.
