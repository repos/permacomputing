Time-sharing
============

**Time-sharing** usually refers to the shared simultaneous use
of a large computer by several users using small computers or
terminals, but the term can be expanded to any shared or
multi-user use of computer hardware.

Server-based time-sharing
-------------------------

This is the oldest type of time-sharing originating from the
decades when computers were very expensive. However, it has
recently returned to prominence of broadband Internet
connections: [[cloud computing]], [[software as a service]],
etc.

The difference is that while the original form represented a
more efficient use of computing resources (and a more
immediate, [[interactive access|character terminal]] to those
resources), the current trend is more closely connected to
corporate interests and "intellectual property". Software that
depends on an external server cannot be pirated as easily. The
network bandwidth requirements often make the use of this kind
of software consume a lot more energy than running equivalent
software locally.

Distributed public computing
----------------------------

This is the idea of using the spare computing power of small
computers to assist in a large-scale computing task –
essentially the opposite of server-based time-sharing.
Seti@Home was an early prominent example of this.

This was a particularly relevant idea when a lot of users had
powerful personal computers that consumed a lot of energy even
when running idle, but nowadays more care is needed when
assessing such projects.

Sharing of computers
--------------------

This refers to the use of a single personal computer by several
people. This may be a computer shared within a family or a
community, or a public computer/terminal located in a place
like an Internet Cafe, a computer classroom or a public
library.

Currently, the industry pushes everyone to own a personal
computing device (especially a smartphone), which is causing a
huge environmental impact which is further multiplied by
planned [[obsolescence]]. This is accompanied by a
psychological impact which is arguably more negative than
positive especially if "online" becomes the default state of
mind. Revitalizing of computer sharing might help tremendously
with these problems.
