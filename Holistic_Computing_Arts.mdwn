**[[Holistic Computing Arts]]** is an artist-centred focus on computing arts that acknowledges the problem of the mesmeric qualities of vast computations and their immeasurable possibilities alongside the troubling ethics of participating in the extractive violence of computing (where over 50 different minerals needed for computer chips are extracted from the earth, often from unceded lands).

[[Holistic Computing Arts]] calls for a reconsidered position on low-emission computational practices by artists working culturally, socially, and critically with increased skills in computational media empowered by this awareness of limitations, which, conversely, increases the potential for creativity.

[[Holistic Computing Arts]] workshops focus on how artists can contribute their expertise to the field rather than being co-opted by the dominant technological and economic discourses that generally inform and drive the conversation.

The discussion reflects on a computer as being a material in the manner of conventional art materials; able to influence, modulate, transform and to be our artistic productions.

Navigating personal computing infrastructure and the incumbent knowledge and conventions that lead to personal expectations, ranging across customs around sending and receiving emails, the provenance of hardware, file formats, directories/folders, the minerals and vessels we keep data in, and the space and lands these may occupy.

Discussions examine the computer as material media (physical, substantial and instrumental) capable of permeating and influencing all segments and layers of contemporary life via our communications, polity, activism, economics, psychological, etc., and reflects on the necessity of understanding the distinction between experience with computing arts as a medium (rather than as an editor for text, audio or visual materials, or third-party social media tools), where the computer performs as a cultural apparatus, where the partial content and action of the artwork are generated. 


Depending on the experience of participants, for the uninitiated, typically included in the commentary is:

* intimate experiences with computing-inscribed social experiences (family photo archives)

* an exchange of the cultural/agential properties inherent in materials/materiality of the computing mediums through which we work and archive our ephemera;

* working with networks and systems from within carries awareness that we operate in an interconnected cultural and ecological sphere in which the computational plays a formative and extractive role; and

* appreciating computing as an art medium brings an awareness of ecological constructions and the social, cultural, and political issues that hover, enabling a tendency to analyse the systems and dynamics that belie people’s artistic outputs.

See also:

* [Holistic Computing Arts Conversation 2023](https://www.autoluminescence.institute/resources/reports/holistic_computing_conversation_2023_report/), [[sister0]], 2023.

* [The Oceanic provenance of Permacomputing and Computational poetics](https://www.autoluminescence.institute/resources/library/intro-oceanic_provenance_permacomputing_codework/), [[sister0]], 2024.

* [VvitchVVave post digital aesthetics symposium](https://www.vvitchvvavve.me/), 2019.

* [Networked Art Forms: Tactical Magick Faerie Circuits](https://sister0.hotglue.me/), a series of events inspired by computer culture, artists, programmers and thinkers from the frontline of the maker aesthetic, devised by [[sister0]], 2013.
