**Niklaus Wirth** (born 1934 in Switzerland) is one of the most prominent computer scientists, mostly known for creating Pascal and several other programming languages.

A lot of Wirth's work is related to concretely proving that [[bloat]] is not a necessity and that smaller and more elegant computing is possible:

The **Lilith** workstation, the design of which started in the late 1970s, was Wirth's idea of a personal computer. It was inspired by the [[Xerox Alto]] but was particularly aimed at students. It was designed to be powerful enough while being simple enough to be understood by a single person (such as a computing student). Lilith is thoroughly built around the
Modula-2 programming language, even the microcode is compiled from a variant of Modula-2. The [[stack-based]] instruction set architecture reached competitive speeds particularly thanks to the high code density, and it also helped keep the Modula-2 compiler fast and simple. Unfortunately, Lilith was a commercial failure, and less than 200 were built.

**[[Oberon]]** is an operating system centered around the
[[object-oriented]] Oberon programming language, with the guiding principles of clarity and simplicity. A major reason to its existence was proving that a full graphical and modern operating system with a software development environment is possible in a moderate size (a couple of hundred kilobytes). It was also used by Wirth as an example of **lean software**. Oberon is still being developed and maintained, and Wirth himself is still involved with the project.

See also:

* [[Wirth's law]]
