Decentralization
================
                              
**Decentralization** refers to distributing activity away from a central, authoritative location.

In computing, a prominent example of [[centralization]] are large online platforms (often related to social media). Decentralization of these includes moving activity from them to smaller servers ([[smallnet]], [[Fediverse]]), as well as moving away from the [[WWW]] towards more [[offline-tolerant|offline first]] or [[peer-to-peer]] protocols.

Decentralization supports [[diversity]] (including [[technological diversity]]) and empowers users to own the technology and services they use.

Decentralization may also go horribly wrong in ways that work against permacomputing goals with radically increased energy use, etc. See the [[cryptocurrency]]/blockchain world for examples.

A useful reflection on the negative aspects of dencentralization is [Problems of Decentralism](https://theanarchistlibrary.org/library/murray-bookchin-the-meaning-of-confederalism#toc2) from The Meaning of Confederalism by Murray Bookchin.

See also:

  * [[file collection]]
  * [[offline first]]
  * [[information battery]]
