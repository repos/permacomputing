# Lumberpunk

**Lumberpunk** is a (not entirely serious) notion of a technological culture that is largely centered around traditional wood-working techniques and forest agriculture. In many heavily wooded places, a majority of objects, vehicles and housing were, until recently, largely made of wood. Instead of buying things made in factories, they were home-made or bought from local craftspeople. 

There are some serious, actual cases of modern "high-tech" objects constructed with wood - satellites, sky-scrapers and even luxury computers.

Unlike steampunk, Lumberpunk valorizes a pre-industrial culture where the value of convenience had not yet started to hollow out our ideas of quality, regional identity and direct connection to nature.

It might be considered a variant of [[Solarpunk]] that is more focused on materials than energy, looking to our pragmatic ancestors more than an aspirational future.

The term was suggested by [notplants](https://sunbeam.city/@notplants) to describe the aesthetics and ideals of [[brendan]]'s more radical experiments and utopian [[Sylvocultue]] dreams.
