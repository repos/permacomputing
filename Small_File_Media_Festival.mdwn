The **Small File Media Festival** is a film festival in Canada, focused on short films with file sizes less than five megabytes.

According to their website, SFMF works in defense of the tiny image. Size matters, and small is better, tiny is best, which is not merely to argue for a different aesthetics or narrative structures (that too) but also for an understanding that all media is media ecology – and as such, directly related to infrastructures with environmental costs.

* [Official website](https://smallfile.ca), sadly, they have a poorly optimized website that fails almost entirely to display on slow-bandwidth and old browsers. The fact that the main page is more than half the size of a five-megabyte film somewhat ruins the point of their activism.
