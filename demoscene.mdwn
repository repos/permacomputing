Demoscene
=========

The **demoscene** is a computer art subculture that creates programmed
audiovisual art in all kinds of programmable devices, often within very
tight constraints. Aspects of the demoscene that may be relevant to
permacomputing include:

  * [[Media minimization]].
  * Small and optimized program code where [[bloat]] and superfluous
    [[dependencies|dependency]] are shunned.
  * The use of limitations and peculiar hardware characteristics as creative
    inspiration.
  * The relationship to old technology. New and old platforms coexist, with
    old platforms having been supported as a continuous tradition rather
    than having been [[rediscovered|retro]]. New things are constantly
    discovered even on classic platforms.
  * It is an example of a pre-[[siliconization]] subculture that still holds
    on some of the pre-siliconization values.

See also:

  * [[Commodore 64]]
  * [[Amiga]]
  * [[ZX Spectrum]]
  * [[pixel art]]
