## TERMS OF SERVICE / CODE OF CONDUCT / PRIVACY

This document is an adaptation of [LURK's TOS.txt](https://lurk.org/TOS.txt).

### General agreements

The permacomputing wiki, IRC, and mailing list, as well as the LowTech/Permacomputing XMPP chatroom are used by people coming from a variety of cultural, ethnic and professional backgrounds. We strive for the permacomputing community to be welcoming to people of these various backgrounds and provide a non-toxic and harassment-free environment. This document is our iterative 3-in-1 Code of Conduct, Terms of Services and Privacy Statement.

By interactive with the permacomputing community you agree to the following:

* Be respectful towards others. This means that we will not tolerate homophobic, transphobic, racist, ableist and sexist slurs and content, even if intended as a joke, or as an ironic remark. That also includes demeaning, belittling or otherwise verbally intimidating communication. In short, any attitudes that may promote the oppression and/or exclusion of historically marginalized groups will not be accepted. Users who violate this rule may get warned once, or banned right away from using our services if their action were clearly ill-intended.

* We talk to and with people rather than about people and/or groups. That means no insinuating, unwelcome, or otherwise toxic comments regarding a person’s lifestyle choices and practices, antagonizing and incendiary generalizations, flaming or edgy remarks at the expense of someone or another community. We understand and appreciate that for some, the internet has became a place to vent and dump their frustrations. While we welcome sarcasm, critique and the need to seek support and alliances while expressing defeating feelings, our services, as well as its local and remote users, are not meant to be your personal punching ball. Users who violate this rule will get warned once, and then banned from using our services.

* Absolutely no harassment, stalking or disclosure of others’ personal details (doxing). Users which violate this rule will be unconditionally banned.

* Similarly, refusing to disengage during an escalating argument, or spamming users with private messages is a form of harassment. Users who violate this rule will get warned once, and then banned from using our services.

* Hate speech, such as, but not limited to: white supremacy, ethnostate advocacy, discussion of national socialism / nazism will not be tolerated. Users who violate this rule will be unconditionally banned.

* Mass-advertising content is prohibited. However we encourage you to share calls for projects/papers, new project announcements of yours, upcoming events and tasteful reminders to your followers of things like Patreon or websites where they can purchase/support your work.

* Media containing sexualized depictions of children (including lolicon) are not allowed. Users who violate this rule will be unconditionally banned.

### Moderation 

If you have trouble with someone violating these rules, [[contact]] us. Do not hesitate to reach out, and do not feel feel like you're being a nuisance when you do, on the contrary!

### Privacy
                                
By using our services you agree that a minimal amount of information about your connection (IP address) will be kept for debugging and maintenance purposes. You are welcome to use a VPN or Tor to connect to our services though. We do not actively store or archive this information beyond the default rotation time of the software we use. We welcome suggestions and practical information to limit data retention to a minimum! Contact us if you have good experience with that. Some of our services use cookies for remembering settings and preferences. We don't track our users, nor do we use analytical software (third-party or self-hosted).

### USUAL CAPSLOCK BOILER PLATE

THE SERVICES USED IN THE PERMACOMPUTING COMMUNITY ARE PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE ADMINS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SERVICES OR THE USE OR OTHER DEALINGS IN THE SERVICES.
