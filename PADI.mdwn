The National Library of Australia's Preserving Access to Digital Information (PADI) initiative aims to provide mechanisms that will help to ensure that information in digital form is managed with appropriate consideration for preservation and future access.

Its objectives are:

* to facilitate the development of strategies and guidelines for the preservation of access to digital information;
* to develop and maintain a web site for information and promotion purposes;
* to actively identify and promote relevant activities; and
* to provide a forum for cross-sectoral cooperation on activities promoting the preservation of access to digital information.

See also:

* [website](https://web.archive.org/web/20010609024402/http://www.nla.gov.au/padi/about.html), on Wayback Machine
* [PADI's Notes on emulation](https://web.archive.org/web/20010625015518/http://www.nla.gov.au/padi/topics/19.html), on Wayback Machine
