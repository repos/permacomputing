In mainstream computing, ease of use is usually implemented as superficial simplicity or **pseudosimplicity**, as an additional layer of complexity that hides the underlying layers. Meanwhile, systems that are actually very simple and elegant are often presented in ways that make them look complex to laypeople.

Feminist (cyberfeminist) critique of simple interfaces and invisibility of underlying infrastructure says this simplicity is just predisposition for mainstream or stereotype use.

See also:

[[Feminist server]]
