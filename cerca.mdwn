**Cerca** is a web forum software built to service small trust-based
communities and focuses on enabling longform, longer-term discussions. It is
architected to minimize maintenance and hosting costs by carefully choosing
which features it supports, how they work, and which features are intentionally
omitted. It features private threads, a flexible user-driven category system,
an RSS feed for receiving news of forum activity, and a web interface for
managing invites, adding and removing users. As a forum software, it also
prioritizes transparency by logging all administrator actions in a view
accessible by all forum members, and post deletion + edits are restricted
solely to post authors.


Relevance to permacomputing
---------------------------

Cerca is built following a [collapse
informatics](https://damaged.bleu255.com/Collapse_Informatics/) approach i.e.
making the best of what we have today for a potentially more depleted tomorrow.
Its development is mindful of permacomputing constraints while simultaneously
weighing them against deployment, user experience, and longer-term developer
interests. In general, it is a forum software that is easy to deploy
(respectful resource utilisation), easy to work on (low cognitive load), and
easy to maintain (few external systems and dependencies).


Limits and challenges
---------------------

Being part of the [[Golang|Go]] ecosystem, it relies mostly on [[Big Tech]]
development infrastructure, and its existence is dependent on upstream changes
from Alphabet/Google. While the platform owners are somewhat careful with
breaking changes, their change in policies have already impacted Cerca by
impacting how dependencies must be kept up to date,
[example](https://github.com/cblgh/cerca/pull/89#issuecomment-2486083652).


Links
-----

- <https://cblgh.org/cerca>
- <https://github.com/cblgh/cerca>

