**Hardware** refers to the material parts of computing equipment, in contrast to [[software]].

When assessing hardware, we should pay attention to possible sustainability problems preventing [[repair]], [[reuse]] and [[reprogramming]]:

* Insufficient [[documentation]]
* Unavailability of [[replacement parts|obsolescence]]
* [[DRM]] locks preventing the running of homebrew software
* Intellectual property and closed-source firmware
* Not [[designed for disassembly|design for disassembly]]

The world is full of abandoned computer hardware, therefore we shouldn't be too picky, and find creative ways to work with even the lousiest pieces of hardware. We shall approach their problems as something to be fixed with hacking, reverse-engineering and activism. Reuse of already existing or old hardware can ease the stress on energetic and mineral-mining impacts of new production (with some exceptions for power-demanding or toxic devices). 
                                                      
Efforts to create new hardware components in biosphere-compatible and/or local ways are worth supporting (although that goal is still far away for microchips). We are particularly interested in what it requires to build specific types of component with minimal industrial dependencies without destroying the biosphere. Links to succesful DIY projects are welcome.

Types of hardware components:

* [[IC]]s
  * [[processors]] (including [[SoC]]s)
  * [[memory]]
  * [[FPGA]]
* [[Single-board computer]]s
* [[displays]]
* [[batteries]] (including [[supercapacitor]]s)
* [[human input devices]]
* [[storage devices]]
* [[radio devices]]
* [[sensors and active peripherals]]
* [[energy sources]]


How is permacomputing hardware different from IoT?

IoT devices are not specifically produced with sustainability in mind. Often IoT is a by-product of corporate business models and extractivist attitude and used in the same way. In permacomputing, hardware, peripheries and energy sources are balanced together to create supporting networks for ecosystems, designed and grown with critical care to every part, human-nature surroundings and commons. 

~~-~~~

See also:

* [[bedrock platform]] (related to software portability and preservation)
