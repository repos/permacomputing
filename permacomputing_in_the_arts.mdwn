Permacomputing in the Arts
==========================

About
-----

The permacomputing in the arts workshop series is part of a postdoc research from [[ugrnm]] exploring to what degree the concept of permacomputing can be broadened and applied to critically revised, sustainable ways of making computing part of art and design education and professional practice. This research will be embedded in the design curriculum of Willem de Kooning Academy, Rotterdam, NL, focused on redefining the role of artists and designers to contribute to future modes of more sustainable organisation and production.

It is part of a larger research and effort to develop new curricula to support artists and designers materialise and contribute to alternative modes of organisation and production based on strong ecological and cooperative/collaborative values. It seeks to reflect on, experiment and  engage with such alternatives while bringing the urgency of the climate crisis and climate justice to art and design education, while remaining critical of technosolutionism and the language of circularity and sustainability.

Issue
-----

For the past decades, art academies have expanded their curricula from traditional fine art practices, such as painting and sculpture, to work with computer technology. It can take the form of an installation, a performance, a graphic design, a data visualisation, and will often use computer technology for the making, publishing, and circulation of the work. However, artists and designers are not educated to assess to which degree their practice could be environmentally problematic and extractive. This is an issue by itself and gets worse once entering both speculative and applied practices that have the ambition to engage with environmental issues. 

For instance, a graphic designer can be commissioned to program an interactive website to explain the energy impact of digital media. If the website is programmed in a way that requires resource-intensive computing in data centres and drains the batteries of the visitor's phone or laptop, then instead of being an exemplary and generative contribution, it risks to remain symbolic, or in a performative contradiction to its stated goal. At worst, it may even be an alibi for environmentally harmful practices and technologies. This is a problem because it seriously weakens and undermines the interdependent relation between the cultural sector, art and design education, and policymaking. It is also a missed opportunity to showcase forward-looking practices that could be both creative and more sustainable.

Permacomputing could promote a transition from a system where creative practitioners use the latest digital tools and media, regardless of environmental consequences, to a more strategic system in which digital tools and media of all generations, are carefully combined, crafted and used to form a less extractive practice.

Workshops & Lectures planning
-----------------------------

- 16 March 2023: instrument vs instrumentalisation
- 25 April 2023: instrument vs instrumentalisation (repeat)
- 31 May 2023: permacomputing aesthetics
- 5 June 2023: permacomputing aesthetics (repeat)
- Autumn 2023: workshops and seminars TBA

Joining the workshop
--------------------

The activities will running IRL in the Netherlands at the Interaction Station of WdKA, no hybrid/blended setup. Some activities will be happening on other locations with partners. For now it is likely that registration will be partly open and partly invite only. If open it will announced on the permacomputing discussion list and on the socials of [[ugrnm]].
