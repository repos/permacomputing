CARDIAC (CARDboard Illustrative Aid to Computation) is a learning aid developed by David Hagelbarger and Saul Fingerman for Bell Telephone Laboratories in 1968 to teach high school students how computers work. 

The kit consists of an instruction manual and a [[paper computer]].

The computer operates in base 10 and has 100 memory cells which can hold signed numbers from 0 to ±999. It has an instruction set of 10 instructions which allows CARDIAC to add, subtract, test, shift, input, output and jump. 

```
INP(Input): take a number from the input card and put it in a memory cell.
CLA(Clear&Add): clear the accumulator and add the contents of a memory cell to the accumulator.
ADD(Add): add the contents of a memory cell to the accumulator.
TAC(Test accumulator): performs a sign test on the contents of the accumulator; if minus, jump to a specified memory cell.
SFT(Shift): shifts the accumulator x places left, then y places right, where x is the upper address digit and y is the lower.
OUT(Output): take a number from the specified memory cell and write it on the output card.
STO(Store): copy the contents of the accumulator into a specified memory cell.
SUB(Subtract): subtract the contents of a specified memory cell from the accumulator.
JMP(Jump): jump to a specified memory cell. 
HRS(Halt&reset): move bug to the specified cell, then stop program execution. 
```

## Relevance to permacomputing

This project, and others like it, were offering a conceptual computer that could be understood in its entirety by a single person. 

See also:

* [On Wikipedia](https://en.wikipedia.org/wiki/CARDboard_Illustrative_Aid_to_Computation)
