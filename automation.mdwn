In **automation**, the essential question is how much human effort the
automation saves in comparison to the requirements of the automation
technology.

Mere laziness does not justify automation: modern households are full of
devices that save relatively little time but waste a lot of energy.
Automation is at its best at continuous and repetitive tasks that require a
lot of time and/or effort from humans but only a neglectable amount of
resources from a programmable device.

[[Permaculture]] wants to develop systems where nature does most of the work,
and humans mostly do things like maintenance, design and building. A good
place for computerized automation would therefore be somewhere between
natural processes and human labor.
