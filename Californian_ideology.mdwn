**Californian ideology** refers to the dominant ideology of Silicon Valley,
as described by the British media theorists Barbrook and Cameron in their
1995 essay "The Californian Ideology". The ideology is very influential
because of the ubiquity of Californian technology products, as well as
because [[siliconization]] has made it prominent in computing industries all
around the world.

Californian ideology combines growth-oriented and individualistic
[[neoliberalism]] with elements from the U.S. counterculture of the
1960s-1970s. It embraces ideas such as cyber-utopianism, techno-determinism,
transhumanism and [[extropianism|maximalism]]. Notably, it often attempts to
constitute holistic world views where computing (as well as economic growth)
has an essential role down to the metaphysical level.

From the permacomputing point of view, Californian ideology can be regarded
as a kind of anti-example – how not to build a holistic world view around
computing and high technology. While some of its root ideas are good
(including the use of computers for self-improvement), they have been
banalized by the growth-obsessive techno-capitalism.

External links:

* [The 2007 revised version of the original essay](http://www.imaginaryfutures.net/2007/04/17/the-californian-ideology-2/)
