Welcome to the Permacomputing wiki!

![permaflower](../pmclogo-neau.png)

**[[Permacomputing]]** is both a concept and a community of practice
oriented around issues of resilience and regenerativity in computer and
network technology inspired by permaculture.

There are huge environmental and societal [[issues]] in today's computing,
and Permacomputing specifically wants to challenge them in the same way as
[[permaculture]] challenges industrial agriculture.

Permacomputing is also a utopian ideal that needs a lot of rethinking,
rebuilding and technical design work to put in practice. This is why a lot
of material on this wiki is highly technical.

For more information:

* [[A longer introduction|Permacomputing]]
* [[Principles]] for designers
* [[Issues]] in today's computing
* [[Wiki]] information

Resources:

* [[Getting Started]] - a starter's manual of some sort
* [[Library]] - texts and media about and around permacomputing
* [[Projects]] - selection of projects affiliated with or adjacent to permacomputing
* [[Assessments]] - review and analysis of existing and established pieces of technology
* [[Communities]] - permacomputing communities of practice and related
* [[Events]] - such as talks, workshops, meetings and seminars
* [[Contribute to the wiki|contribute]] - join us!
