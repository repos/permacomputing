A **calculation factory** is a computer that works like a factory: it has a relatively static material and energy requirements with very little concern or flexibility in regards to the environment they are part of.

Mainframe computers in particular have been thought as factories since the 1950s, including the idea of minimizing their idle time by having their operators work in multiple shifts. This kind of thinking carried over to the Internet server world.

Even small computer systems tend to inherit some characteristics from calculation factories. They often have very poor means to adjust their operation to the changes in physical conditions. This kind of environment-reactivity is an area permacomputing is interested to develop.
