An **operating system** is a piece of software that most crucially handles
the interoperability between hardware and software as well as between
different software programs.

In general, an OS is required in order to run other programs. A program that
is written to use the hardware directly does not require a separate OS, but
this severely limits the possibility of using any other programs in the
hardware. Even embedded systems that are only supposed to run a single fixed
application often run on top of some kind of an OS.

See also:

  * [[Operating systems]] - list of various OSes to be assessed for permacomputing
