##SBC 

load, power (without any peripherals)

###Raspberry (all 5V)

* 4B Idle                                         540 mA (2.7 W)
* 4B ab -n 100 -c 10 (uncached) 	          1010 mA (5.1 W)
* 4B 400% CPU load (stress --cpu 4) 	        1280 mA (6.4 W)
* 3B+ 	HDMI off, LEDs off 	                350 mA (1.7 W)
* 3B+ 	HDMI off, LEDs off, onboard WiFi 	400 mA (2.0 W)
* 3B+    desktop, 1080p video                    510 - 520 mA
* 3B 	HDMI off, LEDs off 	                230 mA (1.2 W)
* 3B 	HDMI off, LEDs off, onboard WiFi 	250 mA (1.2 W)
* 3B     desktop, 1080p video                    360 - 390 mA
* 2B 	HDMI off, LEDs off 	200 mA (1.0 W)
* 2B 	HDMI off, LEDs off, USB WiFi 	240 mA (1.2 W)
* 2B     desktop, 1080p video 350 - 370 mA
* Zero 2 W 	HDMI off, LED off 	100 mA (0.6 W)
* Zero 2 W 	HDMI off, LEDs off, onboard WiFi 	120 mA (0.7 W)
* Zero W   desktop, 1080p video 170 - 270 mA
* Zero 	HDMI off, LED off 	80 mA (0.4 W)
* Zero 	HDMI off, LED off, USB WiFi 	120 mA (0.7 W)
* Zero  1080p video 140 mA
* B+ 	HDMI off, LEDs off 	180 mA (0.9 W)
* B+ 	HDMI off, LEDs off, USB WiFi 	220 mA (1.1 W)
* B+    desktop, 1080p video 240 -340 mA
* A+ 	HDMI off, LEDs off 	80 mA (0.4 W)
* A+ 	HDMI off, LEDs off, USB WiFi 	160 mA (0.8 W)
* A+    desktop, 1080p video 140 -250 mA

###Arduino 

* Uno R3  50 mA (5V)
* Nano 10 - 30 mA (3.3V)
* pro mini 8MHz 5 mA (3.3V)
* pro mini 1MHz 500 uA (1.8V)

##CPU
###ARM Cortex M0+ (32-Bit Single-Core 48MHz 256KB FLASH)

* idle 1.3 mA - 2.4 mA
* simple load 4 - 6 mA


##Radio components

###Bluetooth Low Energy
NINA B3 (Arduino Nano 33) 

* sleep 1 - 2 uA
* connected, standby 8dbm 1 mA (3.3V) 1.6 mA (1.8V)
* TX burst 1kB/10ms 2Mbit 14 mA


Sources:

* https://www.raspberrypi-spy.co.uk/2018/11/raspberry-pi-power-consumption-data/
* https://www.pidramble.com/wiki/benchmarks/power-consumption
* https://raspi.tv/2018/how-much-power-does-raspberry-pi-3b-use-power-measurements
* https://www.iot-experiments.com/arduino-pro-mini-power-consumption/
